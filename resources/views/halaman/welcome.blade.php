<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>welcome</title>
</head>
<body>
    @extends('layout.master')
    @section('judul')
        Halaman Welcome
    @endsection
    @section('content')
    <h1>Selamat Datang {{$nama}} {{$nama2}}</h1>
    <p>Terimakasih telah bergabung bersama kami di SanberBook. Social media kita bersama!</p>
    @endsection
</body>
</html>